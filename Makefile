PDFLATEX=pdflatex
BIBTEX=bibtex
LTX=$(PDFLATEX) -shellescape -interaction=nonstopmode
INKS=inkscape
GIT=git
PDFINFO=pdfinfo
PODOFOBOX=podofobox
GNUPLOT=gnuplot

svgs=$(wildcard imgs/*.svg)
frames=venn-1.pdf venn-2.pdf venn-3.pdf pp-scenarios-1.pdf pp-scenarios-2.pdf adaptive-composition-1.pdf adaptive-composition-2.pdf adaptive-composition-3.pdf adaptive-composition-4.pdf adaptive-composition-5.pdf adaptive-composition-6.pdf architecture-zip-1.pdf architecture-zip-2.pdf architecture-zip-3.pdf adaptive-composition-4.pdf protocol-model-rest.pdf protocol-model-soap.pdf protocol-model-rpc.pdf protocol-model-eos.pdf
pdfs=$(svgs:.svg=.pdf) $(foreach f,$(frames),imgs/$f)
plotsgps=$(wildcard plots/*.gp)
plotspdfs=$(plotsgps:.gp=.pdf)

all: imgs main.pdf

imgs: $(pdfs)

plots: $(plotspdfs)

.PHONY: all imgs plots clean

clean:
	rm -fr _minted-main *.aux *.vrb *.bbl *.blg *.log *.nav *.out *.pdf *.snm *.synctex.gz *.toc auto imgs/*.tmp.pdf {imgs,plots}/*-eps-converted-to.pdf

plots/%.pdf: plots/%.gp plots/%.csv
	cd plots && $(GNUPLOT) ../$<

imgs/%.pdf: imgs/%.svg
	$(INKS) -z -C -A=$@ $< 

#Possible bug: inkscape -z -D -i gSlide1 -A=out.pdf in.svg
#Workaround: generate a pdf of the whole drawing (-D, beyond page borders); get bottom:left:width:height of object; convert px to PostScript pts; crop from the pdf
#Quirk: inkscape -Y has top-left corner of page at origin
define svg2pdf
	$(INKS) -z -D -A=$(3).tmp.pdf $(1).svg
	PTS=$$($(PDFINFO) $(3).tmp.pdf | grep 'Page size' | sed 's/[^0-9]*\([0-9]*\.[0-9]*\)[^0-9].*/\1/'); \
	IW=$$($(INKS) -z --query-width $(1).svg); \
	FAC=$$(echo "scale=3; 100 * $$PTS / $$IW" | bc); \
	X=$$(awk "BEGIN {printf \"%.3f\", $$FAC * $$($(INKS) -z -I $(2) -X $(1).svg)}"); \
	Y=$$(awk "BEGIN {printf \"%.3f\", $$FAC * (-1)*($$($(INKS) -z -I $(2) -Y $(1).svg))}"); \
	W=$$(awk "BEGIN {printf \"%.3f\", $$FAC * $$($(INKS) -z -I $(2) -W $(1).svg)}"); \
	H=$$(awk "BEGIN {printf \"%.3f\", $$FAC * $$($(INKS) -z -I $(2) -H $(1).svg)}"); \
	$(PODOFOBOX) $(3).tmp.pdf $(3).pdf media $$X $$Y $$W $$H
	rm $(3).tmp.pdf
endef

imgs/venn-1.pdf: imgs/venn.svg
	$(call svg2pdf,imgs/venn,gSlide1,imgs/venn-1)
imgs/venn-2.pdf: imgs/venn.svg
	$(call svg2pdf,imgs/venn,gSlide2,imgs/venn-2)
imgs/venn-3.pdf: imgs/venn.svg
	$(call svg2pdf,imgs/venn,gSlide3,imgs/venn-3)

imgs/adaptive-composition-1.pdf: imgs/adaptive-composition.svg
	$(call svg2pdf,imgs/adaptive-composition,gSlide1,imgs/adaptive-composition-1)
imgs/adaptive-composition-2.pdf: imgs/adaptive-composition.svg
	$(call svg2pdf,imgs/adaptive-composition,gSlide2,imgs/adaptive-composition-2)
imgs/adaptive-composition-3.pdf: imgs/adaptive-composition.svg
	$(call svg2pdf,imgs/adaptive-composition,gSlide3,imgs/adaptive-composition-3)
imgs/adaptive-composition-4.pdf: imgs/adaptive-composition.svg
	$(call svg2pdf,imgs/adaptive-composition,gSlide4,imgs/adaptive-composition-4)
imgs/adaptive-composition-5.pdf: imgs/adaptive-composition.svg
	$(call svg2pdf,imgs/adaptive-composition,gSlide5,imgs/adaptive-composition-5)
imgs/adaptive-composition-6.pdf: imgs/adaptive-composition.svg
	$(call svg2pdf,imgs/adaptive-composition,gSlide6,imgs/adaptive-composition-6)

imgs/protocol-model-rest.pdf: imgs/protocol-model.svg
	$(call svg2pdf,imgs/protocol-model,gREST,imgs/protocol-model-rest)
imgs/protocol-model-soap.pdf: imgs/protocol-model.svg
	$(call svg2pdf,imgs/protocol-model,gSOAP,imgs/protocol-model-soap)
imgs/protocol-model-eos.pdf: imgs/protocol-model.svg
	$(call svg2pdf,imgs/protocol-model,gEOS,imgs/protocol-model-eos)
imgs/protocol-model-rpc.pdf: imgs/protocol-model.svg
	$(call svg2pdf,imgs/protocol-model,gRPC,imgs/protocol-model-rpc)

imgs/pp-scenarios-1.pdf: imgs/pp-scenarios.svg
	$(call svg2pdf,imgs/pp-scenarios,gSlide1,imgs/pp-scenarios-1)
imgs/pp-scenarios-2.pdf: imgs/pp-scenarios.svg
	$(call svg2pdf,imgs/pp-scenarios,gSlide2,imgs/pp-scenarios-2)

imgs/architecture-zip-1.pdf: imgs/architecture-zip.svg
	$(call svg2pdf,imgs/architecture-zip,gSlide1,imgs/architecture-zip-1)
imgs/architecture-zip-2.pdf: imgs/architecture-zip.svg
	$(call svg2pdf,imgs/architecture-zip,gSlide2,imgs/architecture-zip-2)
imgs/architecture-zip-3.pdf: imgs/architecture-zip.svg
	$(call svg2pdf,imgs/architecture-zip,gSlide3,imgs/architecture-zip-3)

main.aux: main.tex $(pdfs) $(plotspdfs)
	$(LTX) main.tex || true

main.pdf: main.aux 
	bibtex main.aux; \
	$(LTX) main.tex; \
	$(LTX) main.tex || true

