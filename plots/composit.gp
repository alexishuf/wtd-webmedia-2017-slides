#set terminal qt
#set terminal epslatex size 8.63cm, 3.75cm font ",9"
#set output "composit.tex"
set terminal pdfcairo size 8.63cm, 3.75cm font ",13"
set output "composit.pdf"
#set terminal pdfcairo font ",9" size 8.39099cm,3cm
#set output "composit.pdf"

set style data histogram
set style histogram errorbars gap 2 lw 2

set key on inside left top horizontal samplen 0.5 width -2.8
set xlabel "Problema"
set ylabel "Tempo (escala log_{10})"

set style fill solid border rgb '#000000'
set auto x
set xtics in scale 0, 0 
set logscale y
set yrange [30:*]

set datafile separator ","

plot 'composit.csv' \
        using (column("c.med")):(column("c.med.error")):xtic(1) title "ComposIT" lc rgb '#e0e0e0', \
     '' using (column("a.med")):(column("a.med.error")):xtic(1) title "Protótipo" lc rgb '#808080'
