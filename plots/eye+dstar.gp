#set terminal epslatex size 8.63cm, 3.75cm font ",10"
#set terminal epslatex size 6.90cm, 3.00cm font ",10" #80%
#set terminal epslatex size 6.47cm, 3.00cm font ",8"  #75%
#set output "eye.tex"
set terminal pdfcairo size 6.47cm, 3.50cm font ",12"  #75%
set output "eye+dstar.pdf"


unset key
set xrange [1.5:1024+256]
set xtics rotate by 45 right
set yrange[0.25:]
set logscale x 2
set logscale y
set xlabel "n (escala log_2)"    offset 0  ,1 ,0
set ylabel "δ_E (esc. log_{10})" offset 3.5,-2,0
set rmargin 0.9
set lmargin 7
set tmargin 1
set bmargin 3.5
set key inside top left Left reverse

set datafile separator ","
plot 'eye+dstar.csv' using (column("n")):(column("eye")) with points pt 7 ps 0.75 lc rgb '#000000' title 'EYE', \
     '' using (column("n")):(column("dstar")) with points pt 7 ps 0.75 lc rgb '#ff0000' title 'D* Lite'
