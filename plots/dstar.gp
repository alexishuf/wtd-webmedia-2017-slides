#set terminal epslatex size 8.63cm, 3.75cm font ",10"
#set terminal epslatex size 6.90cm, 3.00cm font ",10" #80%
#set terminal epslatex size 6.47cm, 3.00cm font ",10"  #75%
#set output "dstar.tex"
set terminal pdfcairo size 6.47cm, 3.50cm font ",12"  #75%
set output "dstar.pdf"

unset key
set xrange [1.5:1024+256]
#set xtics (2, 64, 256, 512, 1024)
set ytics 20 textcolor rgb '#ff0000'
set xtics rotate by 45 right
#set yrange[1:]
set logscale x 2
#set logscale y
set xlabel "n (escala log_2)" offset 0,1,0
set ylabel "δ_E"      offset 1,0,0
set rmargin 0.9
set lmargin 3.5
set tmargin 1
set bmargin 3.5

set datafile separator ","
plot 'dstar.csv' using (column("n")):(column("t")) with points pt 7 ps 0.75 lc rgb '#000000'
