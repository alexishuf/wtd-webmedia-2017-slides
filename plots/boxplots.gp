#set terminal qt
#set terminal epslatex size 8.63cm, 3.75cm font ",9"
#set output "composit.tex"
set terminal pdfcairo size 8.63cm, 3.75cm font ",13"
set output "boxplots.pdf"
#set terminal pdfcairo font ",9" size 8.39099cm,3cm
#set output "composit.pdf"

set key on inside left Left top vertical reverse samplen 0.5 width -2.8
set xlabel "Problema"
set ylabel "Tempo (escala log_{10})"

set bars 4
set style fill solid border rgb '#000000'
set xrange [0.5:9]
set xtics in scale 0, 0 
set logscale y
set yrange [30:*]

set datafile separator ","

plot 'boxplots.csv' \
        using (column("problem")):(column("c.q2")):(column("c.q1")):(column("c.q4")):(column("c.q3")) with candlesticks title "ComposIT" lc rgb '#e0e0e0' whiskerbars, \
     '' using (column("problem")+0.40):(column("a.q2")):(column("a.q1")):(column("a.q4")):(column("a.q3")) with candlesticks title "Protótipo" lc rgb '#808080' whiskerbars
