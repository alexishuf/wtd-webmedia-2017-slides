#set terminal epslatex size 8.63cm, 3.75cm font ",10"
#set terminal epslatex size 6.90cm, 3.00cm font ",10" #80%
#set terminal epslatex size 6.47cm, 3.00cm font ",8"  #75%
#set output "eye.tex"
set terminal pdfcairo size 6.47cm, 3.50cm font ",12"  #75%
set output "astar-inc-density.pdf"


set zeroaxis
unset key
set xtics #rotate by 45 right
set yrange [-0.002:]
set xrange [-500:500]
set xlabel "Protótipo - ComposIT"  offset 0 ,0.5 ,0
set rmargin 1
set lmargin 1
set tmargin 0.5
set bmargin 2.8

set label "80% -103  " at -103,0.009 right point
set label " 130" at 130,0.009 left point
set label "90% -207  " at -207,0.005 right point
set label " 218" at 218,0.005 left point

set datafile separator ","
plot 'astar-inc-density.csv' using (column("inc")):(column("density")) with lines lc rgb '#000000'
